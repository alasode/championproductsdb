﻿---selecting database id and name 
;WITH cte (database_id,[name]) AS
   (  
     SELECT database_id
			,[name]
       FROM sys.databases 
    )  
SELECT database_id
      ,[name]
  FROM cte


---selecting all tables with all fields of the tables 
;WITH cte (TableName, ColumnName) AS
   (  
     SELECT O.[name] AS TableName
	       ,C.[name] AS ColumnName
       FROM sysobjects AS O
	  INNER JOIN syscolumns AS C
	        ON (O.id = C.id)
	  WHERE O.xtype = 'U'
	  --GROUP BY O.[name]
    )  
SELECT TableName
      ,ColumnName
FROM cte