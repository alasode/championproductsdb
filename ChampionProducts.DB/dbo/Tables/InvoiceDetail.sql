﻿CREATE TABLE [dbo].[InvoiceDetail] (
    [InvoiceDetailId] BIGINT          IDENTITY (1, 1) NOT NULL,
    [InvoiceId]       BIGINT          NOT NULL,
    [ProductId]       INT             NOT NULL,
    [Amount]          DECIMAL (18, 2) NOT NULL,
    [CreatedDate]     DATETIME        CONSTRAINT [DF_InvoiceDetail_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [DeletedDate]     DATETIME        NULL,
    CONSTRAINT [PK_InvoiceDetail] PRIMARY KEY CLUSTERED ([InvoiceDetailId] ASC),
    CONSTRAINT [FK_InvoiceDetail_Invoice] FOREIGN KEY ([InvoiceId]) REFERENCES [dbo].[Invoice] ([InvoiceId]),
    CONSTRAINT [FK_InvoiceDetail_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([ProductId])
);






GO
CREATE NONCLUSTERED INDEX [IX_InvoiceDetail]
    ON [dbo].[InvoiceDetail]([InvoiceDetailId] ASC);

