﻿CREATE TABLE [dbo].[Address] (
    [AddressId] INT           NOT NULL,
    [Street]    VARCHAR (100) NOT NULL,
    [City]      VARCHAR (50)  NOT NULL,
    [Zip]       VARCHAR (5)   NOT NULL,
    [State]     CHAR (2)      NOT NULL,
    CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED ([AddressId] ASC),
    CONSTRAINT [FK_Address_Address] FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Address] ([AddressId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Unique_Address]
    ON [dbo].[Address]([Street] ASC, [City] ASC, [State] ASC, [Zip] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Address]
    ON [dbo].[Address]([Street] ASC, [City] ASC, [State] ASC, [Zip] ASC);

