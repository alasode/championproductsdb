﻿CREATE TABLE [dbo].[CustomerType] (
    [CustomerTypeId] INT          IDENTITY (1, 1) NOT NULL,
    [Description]    VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_CustomerType] PRIMARY KEY CLUSTERED ([CustomerTypeId] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_CustomerType]
    ON [dbo].[CustomerType]([Description] ASC);

