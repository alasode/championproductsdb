﻿CREATE TABLE [dbo].[Customer] (
    [CustomerId]     BIGINT        IDENTITY (1, 1) NOT NULL,
    [CustomerTypeId] INT           NOT NULL,
    [BusinessName]   VARCHAR (200) NULL,
    [LastName]       VARCHAR (50)  NOT NULL,
    [FirstName]      VARCHAR (50)  NULL,
    [AddressId]      INT           NULL,
    [CreatedDate]    DATETIME      CONSTRAINT [DF_Customer_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [IsEnabled]      BIT           NOT NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([CustomerId] ASC),
    CONSTRAINT [FK_Customer_Address] FOREIGN KEY ([CustomerTypeId]) REFERENCES [dbo].[CustomerType] ([CustomerTypeId])
);






GO
CREATE NONCLUSTERED INDEX [IX_Customer]
    ON [dbo].[Customer]([FirstName] ASC, [LastName] ASC);

