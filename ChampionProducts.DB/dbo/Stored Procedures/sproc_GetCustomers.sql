﻿-- =============================================
-- Author:		Victoria Lasode
-- Create date: 04/18/2021
-- Description:	Get Customer info
-- exec sproc_GetCustomers
-- =============================================

CREATE PROCEDURE [dbo].[sproc_GetCustomers] 
	@customerId bigint = null
AS
BEGIN

	SET NOCOUNT ON;

	  SELECT  FirstName, 
			  LastName, 
			  BusinessName
		FROM  dbo.Customer
	   WHERE  @customerId is null OR CustomerId = @customerId
	ORDER BY LastName

END